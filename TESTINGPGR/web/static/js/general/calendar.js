
$(document).ready(function () {
    $("#nextbutton").hide();

    $('#nextbutton').click(function () {
        $('#calendar').fullCalendar('next');
        $("#prevbutton").show();
        $("#nextbutton").hide();
    });

    $('#prevbutton').click(function () {
        $('#calendar').fullCalendar('prev');
        $("#nextbutton").show();
        $("#prevbutton").hide();
    });



    var aEventos = [];
    const STARTCOLOR = "#3C8DBC", ENDCOLOR = "#01FF70", BADSTART = "#DD4B39", BADEND = "#DD4B39", OVERTIMECOLOR = "#f4c20d";

    $.ajax({
        dataType: "json",
        error: function (data) {
            console.log(data);
        },
        success: function (data) {
            fnProcessDates(data.rows);
            startCalendar();
        },
        type: "get",
        url: "/TESTINGPGR/jsp/Clima/marcaciones.json"
    });

    function fnProcessDates(events) {
        var sPrevious = "", dPrevious;
        $.each(events, function (i, e) {
        	debugger;
            var dCurrent = new Date(e.cell[2]), color = BADSTART, bAllDayEvent = false;
            // Olvidó marcación por la mañana
            if (sPrevious !== "" && sPrevious === "morning" && e.cell[6] === "morning") {
                var aPrevious = dPrevious.split(" ");
                aEventos.push({
                    color: color,
                    title: "NO MARCACION",
                    start: aPrevious[0] + " 16:00:00.0"
                });
            }
            // Olvidó marcación por la tarde
            if (sPrevious !== "" && sPrevious === "TARDE" && e.cell[6] === "TARDE") {
                dPrevious = e.cell[2];
                var aPrevious = dPrevious.split(" ");
                aEventos.push({
                    color: color,
                    title: "NO MARCACION",
                    start: aPrevious[0] + " 08:00:00.0"
                });
            }
            sPrevious = e.cell[6], dPrevious = e.cell[2];
            if (e.cell[6] === "morning") {
            	if (dCurrent.getHours() < 8 || (dCurrent.getHours() === 8 && dCurrent.getMinutes() === 0)) {
            		color = STARTCOLOR;
            	}
            	if (dCurrent.getHours() > 8 || (dCurrent.getHours() === 8 && dCurrent.getMinutes() > 0)) {
            		color = BADSTART;
            	}
            }
            if (e.cell[6] === "TARDE") {
            	if (dCurrent.getHours() === 16 && dCurrent.getMinutes() <= 30) {
            		color = ENDCOLOR;
            	}
            	if (dCurrent.getHours() < 16) {
            		color = BADSTART;
            	}
            	if (dCurrent.getHours() > 16 || (dCurrent.getHours() === 16 && dCurrent.getMinutes() > 30)) {
            		color = OVERTIMECOLOR;
            	}
            }
            // No marcó en todo el día
            if (e.cell[6] === "NO MARCA") {
							bAllDayEvent = !bAllDayEvent;
							aEventos.push({
									allDay: bAllDayEvent,
							    color: color,
							    title: (bAllDayEvent) ? "NO MARCACION" : "",
							    start: e.cell[2]
							});
							aEventos.push({
									allDay: bAllDayEvent,
							    color: color,
							    title: (bAllDayEvent) ? "NO MARCACION" : "",
							    start: e.cell[2]
							});
            } else {
	            aEventos.push({
	            		allDay: bAllDayEvent,
	                color: color,
	                title: (bAllDayEvent) ? "NO MARCACION" : "",
	                start: e.cell[2]
	            });
            }
        });
    }

    function startCalendar() {
        $('#calendar').fullCalendar({
        		lang: "es",
            header: {
                left: '',
                center: 'title',
                right: 'Mes'
            },
            // customize the button names,
            // otherwise they'd all just say "list"
            views: {
                type: 'month'
            },
            viewRender: function (view, element) {
                var start;
                var end = moment();
                var d_start = end.format('DD');
                var m_start = end.format('MM') - 1;
                var y_start = end.format('YYYY');

                start = d_start + '-' + m_start + '-' + y_start;

                view.start = start;
                view.end = moment();
            },
            defaultView: 'month',
            defaultDate: moment(),
            navLinks: false, // can click day/week names to navigate views
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            events: aEventos,
            timeFormat: "h:mm"
        });
    }
});