/* 
 * Williams Ramos
 */

function cagarAutocompleteBasico(servlet, opcion,elementoAutocomplete, elementoHidden ) {
    var vectorCargar = new Array();
    $.ajax({
        type: "POST",
        url: "/SERH/PGR/" + servlet + "?opcion=" + opcion,
        dataType: 'json',
        success: function (data) {
            /* verificar la cantidad de datos*/
            if (data.rows.length == 1) {
                alert("ha ocurrido un error inesperado. Por favor contactese con el Administrador del Sistema Informatico. Codigo ERROR: S8");
            } else {
                var dataJson = data;
                $.each(data.rows, function (i, rows) {
                    vectorCargar[i] = rows.cell[1];
                });
                $("#"+elementoAutocomplete).autocomplete({
                    source: vectorCargar,
                    minLength: 3,
                    select: function (event, ui) {
                        var seleccionItem = ui.item.value;
                        /*comparar para determinar el codigo de la pretension el cual sera almaceando*/
                        $.each(dataJson.rows, function (j, rows) {
                            if (seleccionItem == rows.cell[1]) {
                                /*Esta busqueda permite la seleccion del codigo */
                                $("#"+elementoHidden).val(rows.cell[0]);

                            }
                        });

                    }
                });

            }
        },
        error: function (data) {
            /* Mensajes de alerta */
            alertarMensaje();
            /*colocar un identificador en la pantalla para decir que existe un error*/
        }

    });
}

function alfanumerico(campo){
    $('#'+campo).bind('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z Ññüáéíó]");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

}

