/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$("#slTipoReporteId").change(function () {
    var str = "";
    $("#slTipoReporteId option:selected").each(function () {
        str += $(this).text() + " ";
    });
//alert(str);
}).trigger('change');

$("#cmdReporteIndividual").button().click(function (e) {
    e.preventDefault();
    var seleccionGrid = $("#tbEmpleado").jqGrid('getGridParam', 'selrow');
    var nombreEmpleado = $("#tbEmpleado").jqGrid('getCell', seleccionGrid, 'NombreEmpleado');
    var apellidoEmpleado = $("#tbEmpleado").jqGrid('getCell', seleccionGrid, 'ApellidoEmpleado');
    if (nombreEmpleado != "" && apellidoEmpleado != "") {
        window.open("/SERH/PGR/ReportePersonal?nombreEmpleado=" + nombreEmpleado + "&&apellidoEmpleado=" + apellidoEmpleado);

    } else {
        alert("Por favor seleccione un nombre de la lista de Empleados y luego presione Generar Reporte");
    }
});

$("#cerrarNoticia").click(function () {
    removerMensajeAdvertencia();
    removerMensajeError();
});


//$("#cmdReporteUnidad").button().click(function (e) {
//    e.preventDefault();
//    alert("test!");
//    var unidad = $("#tbEmpleado").jqGrid('getCell', seleccionGrid, 'NombreEmpleado');
//    var procuraduria = $("#tbEmpleado").jqGrid('getCell', seleccionGrid, 'NombreEmpleado');
//    if(unidad!="" && procuraduria!=""){
//        window.open("/SERH/PGR/ReporteConsolidado");
//        
//    }else{
//        alert("Es necesario seleccionar una UNIDAD en el listado de procuradurias");
//    }


////////////////    $.ajax({
////////////////        type: "POST",
////////////////        url: "/SERH/PGR/MigradorFormularioExcel",
////////////////        dataType: 'json',
////////////////        success: function(data){
////////////////            alert("con toodo!");
////////////////        },
////////////////        error:function(){
////////////////            alert("Error!!!");
////////////////        }
////////////////    });
//});

$("#cmdReporteProcuraduria").hide().button().click(function (e) {
    e.preventDefault();
    alert("Reporte!");
    //window.open("/SERH/PGR/ReporteConsolidado");
});

function generarImpresion() {
    var seleccionGrid = $("#tbEmpleado").jqGrid('getGridParam', 'selrow');
    var nombreEmpleado = $("#tbEmpleado").jqGrid('getCell', seleccionGrid, 'NombreEmpleado');
    var apellidoEmpleado = $("#tbEmpleado").jqGrid('getCell', seleccionGrid, 'ApellidoEmpleado');
    if (nombreEmpleado != "" && apellidoEmpleado != "") {
        window.open("/SERH/PGR/ReportePersonal?nombreEmpleado=" + nombreEmpleado + "&&apellidoEmpleado=" + apellidoEmpleado + "&opcion=normal");
    } else {
        alert("Por favor seleccione un nombre de la lista de Empleados y luego presione Generar Reporte");
    }
}
function generarImpresionEvaluados() {
    var seleccionGrid = $("#tbEmpleadoEvaluado").jqGrid('getGridParam', 'selrow');
    var codigoEmpleado = $("#tbEmpleadoEvaluado").jqGrid('getCell', seleccionGrid, 'CODIGOEMPLEADO');
    var fechaEvaluacion = $("#tbEmpleadoEvaluado").jqGrid('getCell', seleccionGrid, 'FechaEvaluacion');
    var fechaSistema = $("#tbEmpleadoEvaluado").jqGrid('getCell', seleccionGrid, 'FECHASISTEMA');
    var tipoEvaluacion = $("#tbEmpleadoEvaluado").jqGrid('getCell', seleccionGrid, 'TIPOEVALUACION');


    if (codigoEmpleado != "") {
//        window.open("/SERH/PGR/ReportePersonal?codigoEmpleado=" + codigoEmpleado + "&opcion=empleadoEvaluado&fechaEvaluacion=" + fechaEvaluacion+"&fechaSistema="+fechaSistema+"&tipoEvaluacion="+tipoEvaluacion);
        window.open("/SERH/PGR/ReporteFORHED?codigoEmpleado=" + codigoEmpleado + "&opcion=empleadoEvaluado&fechaEvaluacion=" + fechaEvaluacion + "&fechaSistema=" + fechaSistema + "&tipoEvaluacion=" + tipoEvaluacion);
    } else {
        alert("Por favor seleccione un nombre de la lista de Empleados y luego presione Generar Reporte");
    }
}

function generarConsolidado() {
    
    var seleccionGrid = $("#tbUnidad").jqGrid('getGridParam', 'selrow');
    var procuraduriaUnidad = $("#tbUnidad").jqGrid('getCell', seleccionGrid, 'Num');
    var nombreUnidad = $("#tbUnidad").jqGrid('getCell', seleccionGrid, 'NombreUnidadOrganizacional');
    var nombreProcuraduria = $("#tbUnidad").jqGrid('getCell', seleccionGrid, 'NombreRelacionUnidadOrganizacional');
    if (procuraduriaUnidad != "") {
        //alert("procuraduria: "+procuraduria+" unidad: "+unidad);
        window.open("/SERH/PGR/ReporteConsolidado?procuraduriaUnidad=" + procuraduriaUnidad +"&nombreUnidad="+nombreUnidad+"&nombreProcuraduria="+nombreProcuraduria);
    } else {
        alert("Por favor seleccione una unidad de la lista para generar el reporte consolidado");
    }

}

//$("#cmdGenerarReporte").button().click(function(e){
//    e.preventDefault();
//    //alert("generar reporte");//
//
//    window.open("/SERH/PGR/ReportePersonal");
////    $.ajax({
////        type:"POST",
////        url:"",
////        dataType:"",
////        success: function(){},
////        error: function(){}
////    });
//
//});

/* grid para busqueda de personas */
$("#tbEmpleado").jqGrid({
    url: '/SERH/PGR/ConsultasReportePersonal?opcion=empleado',
    datatype: "json",
    colNames: ['Nombre Total', 'Codigo', 'Nombre', 'Apellido', 'Procuraduria Auxiliar', 'Unidad', 'Cargo'],
    colModel: [
        {
            name: 'EMPLEADO',
            index: 'EMPLEADO',
            hidden: true,
            width: 20
        },
        {
            name: 'CODIGOEMPLEADO',
            index: 'CODIGOEMPLEADO',
            hidden: true,
            width: 20
        },
        {
            name: 'NombreEmpleado',
            index: 'NombreEmpleado',
            hidden: false,
            width: 70
        },
        {
            name: 'ApellidoEmpleado',
            index: 'ApellidoEmpleado',
            width: 70
        },
        {
            name: 'ProcuraduriaEmpleado',
            index: 'ProcuraduriaEmpleado',
            width: 80
        },
        {
            name: 'UNIDADEMPLEADO',
            index: 'UNIDADEMPLEADO  ',
            width: 110
        },
        {
            name: 'CARGOEMPLEADO',
            index: 'CARGOEMPLEADO',
            hidden: false,
            width: 90
        }
    ],
    rowNum: 2000,
    rowList: [2000, 4000, 5000],
    pager: '#barraEmpleado',
    sortname: 'id',
    height: 'auto',
    //height: "100%",//ajustar tamaño a la cantidad de filas
    autowidth: true,
    viewrecords: true,
    ondblClickRow: function (rowid) {
        //jQuery(this).jqGrid('editGridRow', rowid);
        generarImpresion();
    },
    grouping: true,
    groupingView: {
        groupField: ['ProcuraduriaEmpleado'],
        groupColumnShow: [true],
        groupText: ['<b>Procuraduria: {0} - {1} Empleado(s)</b>'],
        groupCollapse: true,
        groupOrder: ['asc'],
        groupDataSorted: true
    },
    sortorder: "desc",
    caption: "Lista de Empleados de la PGR"
});
jQuery("#tbEmpleado").jqGrid('navGrid', '#barraEmpleado', {
    edit: false,
    add: false,
    del: false
});

/* grid para busqueda de procuradurias */
$("#tbProcuraduria").jqGrid({
    url: '/SERH/PGR/ConsultasReportePersonal?opcion=procuraduria',
    datatype: "json",
    colNames: ['Nombre de Procuraduria', 'Cantidad de Empleados Evaluados'],
    colModel: [
        {
            name: 'NombreProcuraduria',
            index: 'NombreProcuraduria',
            hidden: false,
            align: "left",
            width: 200
        },
        {
            name: 'CantidadEmpleados',
            index: 'CantidadEmpleados',
            align: "center",
            width: 100
        }
    ],
    rowNum: 50,
    rowList: [50, 100],
    //pager: '#barraProcuraduria',
    sortname: 'id',
    height: 'auto',
    //height: "100%",//ajustar tamaño a la cantidad de filas
    autowidth: false,
    viewrecords: true,
    sortorder: "desc",
    caption: "Lista de Procuradurias y cantidad de empleados evaluados"
});
jQuery("#tbProcuraduria").jqGrid('navGrid', '#barraProcuraduria', {
    edit: false,
    add: false,
    del: false
});

///* grid para busqueda de unidades de procuraduria UTILIZADO PARA ANTIGUAS REGISTROS A 2015 */
//$("#tbUnidad").jqGrid({
//    url: '/SERH/PGR/ConsultasReportePersonal?opcion=unidad',
//    datatype: "json",
//    colNames: ['No.', 'Procuraduria', 'Nombre de Unidad'],
//    colModel: [
//        {
//            name: 'NUM',
//            index: 'NUM',
//            hidden: true,
//            align: "center",
//            width: 25
//        },
//        {
//            name: 'PROCURADURIAEMPLEADO',
//            index: 'PROCURADURIAEMPLEADO',
//            hidden: false,
//            align: "center",
//            width: 400
//        },
//        {
//            name: 'UNIDADEMPLEADO',
//            index: 'UNIDADEMPLEADO',
//            align: "left",
//            width: 400
//        }
//    ],
//    rowNum: 500,
//    rowList: [500, 250, 100],
//    //pager: '#barraUnidad',
//    sortname: 'id',
//    height: "100%", //ajustar tamaño a la cantidad de filas
//    ondblClickRow: function (rowid) {
//        //alert(jQuery(this).jqGrid('editGridRow', rowid));
//        generarConsolidado();
//
//    },
//    grouping: true,
//    groupingView: {
//        groupField: ['PROCURADURIAEMPLEADO'],
//        groupColumnShow: [false],
//        groupText: ['<b>{0} - {1}  Unidades</b>'],
//        groupCollapse: true,
//        groupOrder: ['desc']
//    },
//    autowidth: false,
//    viewrecords: true,
//    sortorder: "desc",
//    caption: "Lista de Unidades por Procuraduria"
//});
//jQuery("#tbUnidad").jqGrid('navGrid', '#barraUnidad', {
//    edit: false,
//    add: false,
//    del: false
//});


/* grid para busqueda de unidades de procuraduria */
$("#tbUnidad").jqGrid({
    url: '/SERH/PGR/ConsultasReportePersonal?opcion=unidad',
    datatype: "json",
    colNames: ['Num','Procuraduria','Unidad'],
    colModel: [
        {
            name: 'Num',
            index: 'Num',
            hidden: true,
            align: "center",
            width: 25
        },
        {
            name: 'NombreUnidadOrganizacional',
            index: 'NombreUnidadOrganizacional',
            hidden: false,
            align: "center",
            width: 400
        },
        {
            name: 'NombreRelacionUnidadOrganizacional',
            index: 'NombreRelacionUnidadOrganizacional',
            align: "left",
            hidden: false,
            width: 400
        }
    ],
    rowNum: 500,
    rowList: [500, 250, 100],
    //pager: '#barraUnidad',
    sortname: 'id',
    height: "100%", //ajustar tamaño a la cantidad de filas
    ondblClickRow: function (rowid) {
        //alert(jQuery(this).jqGrid('editGridRow', rowid));
        generarConsolidado();

    },
    grouping: true,
    groupingView: {
        groupField: ['NombreUnidadOrganizacional'],
        groupColumnShow: [false],
        groupText: ['<b>{0} - {1}  Unidades</b>'],
        groupCollapse: true,
        groupOrder: ['desc']
    },
    autowidth: false,
    viewrecords: true,
    sortorder: "desc",
    caption: "Lista de Unidades por Procuraduria"
});
jQuery("#tbUnidad").jqGrid('navGrid', '#barraUnidad', {
    edit: false,
    add: false,
    del: false
});




$("#tbEmpleadoEvaluado").jqGrid({
    url: '/SERH/PGR/ConsultasReportePersonal?opcion=empleadoEvaluado',
    datatype: "json",
    colNames: ['CodigoAux', 'Codigo', 'NR', 'Usuario', 'Nombre', 'Apellido', 'Sexo', 'Puesto', 'Unidad', 'Modificacion', 'CodigoPA', 'Procuraduria', 'Registro', 'Fecha Evaluacion', 'Tipo'],
    colModel: [
        {
            name: 'CODIGOAUX',
            index: 'CODIGOAUX',
            hidden: true,
            width: 20
        }, {
            name: 'CODIGOEMPLEADO',
            index: 'CODIGOEMPLEADO',
            hidden: true,
            width: 100
        },
        {
            name: 'identificacioninstitucion',
            index: 'identificacioninstitucion',
            hidden: true,
            width: 20
        },
        {
            name: 'UsuarioSIG',
            index: 'UsuarioSIG',
            hidden: true,
            width: 20
        },
        {
            name: 'NombresPersonaNatural',
            index: 'NombresPersonaNatural',
            hidden: false,
            width: 15
        },
        {
            name: 'ApellidosPersonaNatural',
            index: 'ApellidosPersonaNatural',
            hidden: false,
            width: 15
        },
        {
            name: 'SexoPersonaNatural',
            index: 'SexoPersonaNatural',
            hidden: false,
            width: 5
        }, {
            name: 'NombrePuesto',
            index: 'NombrePuesto',
            hidden: false,
            width: 15
        },
        {
            name: 'NombreRelacionUnidadOrganizacional',
            index: 'NombreRelacionUnidadOrganizacional',
            width: 15
        },
        {
            name: 'Modificacion',
            index: 'Modificacion',
            hidden: true,
            width: 5
        },
        {
            name: 'CodigoUnidadOrganizacional1',
            index: 'CodigoUnidadOrganizacional1',
            width: 5,
            hidden: true
        },
        {
            name: 'NombreUnidadOrganizacional',
            index: 'NombreUnidadOrganizacional',
            hidden: false,
            width: 5
        },
        {
            name: 'FECHASISTEMA',
            index: 'FECHASISTEMA',
            hidden: false,
            width: 7
        },
        {
            name: 'FechaEvaluacion',
            index: 'FechaEvaluacion',
            hidden: false,
            width: 7
        }
        ,
        {
            name: 'TIPOEVALUACION',
            index: 'TIPOEVALUACION',
            hidden: false,
            width: 6
        }
    ],
    rowNum: 2000,
    rowList: [2000, 4000, 5000],
    pager: '#barraEmpleadoEvaluado',
    sortname: 'id',
    height: 'auto',
    //height: "100%",//ajustar tamaño a la cantidad de filas
    autowidth: true,
    viewrecords: true,
    grouping: true,
    ondblClickRow: function (rowid) {
        //jQuery(this).jqGrid('editGridRow', rowid);
        //alert(rowid);
        generarImpresionEvaluados();
    },
    groupingView: {
        groupField: ['NombreUnidadOrganizacional'],
        groupColumnShow: [false],
        groupText: ['<b>{0} - {1}  Registros</b>'],
        groupCollapse: true,
        groupOrder: ['desc']
    },
    sortorder: "desc",
    caption: "Lista de Empleados evaluados mediante el sistema SERH"
});
jQuery("#tbEmpleadoEvaluado").jqGrid('navGrid', '#barraEmpleadoEvaluado', {
    edit: false,
    add: false,
    del: false
});

$("#cmdEliminarEvaluacion").button();
$("#cmdEliminarEvaluacion").click(function (e) {
    e.preventDefault();
    var seleccionGrid = $("#tbEmpleadoEvaluado").jqGrid('getGridParam', 'selrow');
    var codigoEmpleado = $("#tbEmpleadoEvaluado").jqGrid('getCell', seleccionGrid, 'CODIGOEMPLEADO');
    if (codigoEmpleado !== "") {
        mostrarMensajeFlotante("Eliminando registro por favor espere...");

        $.ajax({
            type: "POST",
            url: "/SERH/PGR/IngresoFormulario?opcion=eliminarEvaluacion&txtCodigoEmp=" + codigoEmpleado,
            dataType: "",
            success: function (data) {
                $("#divCargando").html("");
                if (data.accion == "1") {
                    mostrarMensajeFlotante("La evaluacion se ELIMINADO correctamente");
                    $("#divCargando").html("");
                    
                    jQuery("#tbEmpleadoEvaluado").jqGrid().setGridParam({
                        url: '/SERH/PGR/ConsultasReportePersonal?opcion=empleadoEvaluado',
                    }).trigger("reloadGrid");
                    
                } else if (data.accion == "0") {
                    $("#divCargando").html("EXISTEN PROBLEMAS CON EL SERVIDOR");
                }
            },
            error: function () {
                alert("Al parecer hubo un error en el servidor por favor contacte al Administrador");
            }
        });
    }else{
        mostrarMensajeFlotante("Es necesario seleccionar un empleado/a para eliminar la evaluacion.");
        
    }
});

function mostrarMensajeFlotante(mensaje) {
    $("#divNoticia").addClass("ui-state-highlight ui-corner-all");
    $("#iconoNoticia").addClass("ui-icon ui-icon-info");
    $("#cerrarNoticia").addClass("ui-icon ui-icon-circle-close");
    $("#textoNoticia").html(mensaje);
    $("#divInformativo").show().fadeIn(10000, function () {
//        $("#divInformativo").fadeOut(9000);
//        //        removerMensajeOK();
//        $("#divNoticia").removeClass("ui-state-highlight ui-corner-all");
//        $("#iconoNoticia").removeClass("ui-icon ui-icon-info ui-icon-alert");
//        $("#cerrarNoticia").removeClass("ui-icon ui-icon-circle-close");
//        $("#textoNoticia").html(mensaje);
    });
}

function removerMensajeAdvertencia() {
    $("#divNoticia").removeClass("ui-state-highlight ui-corner-all");
    $("#iconoNoticia").removeClass("ui-icon ui-icon-info ui-icon-alert");
    $("#cerrarNoticia").removeClass("ui-icon ui-icon-circle-close");
    $("#textoNoticia").html("")
    $("#divInformativo").hide();
}

//  $.ajax({
//        type:"POST",
//        url:"/SERH/PGR/ConsultasReportePersonal?opcion=ingresar&"+$("#rene input").serialize()+"&formulario=EP04A",
//        dataType:"",
//        success: function(){
//            alert("rene se fue con todo!");
//        },
//        error: function(){
//            alert("ups!");
//        }
//    });

