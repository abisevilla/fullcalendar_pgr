/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$("#txtFechaEvaluado").prop("disabled", true);
$("#txtFechaEvaluado").datepicker({
    //Configuración en Español
    showAnim: "clip",
    showOn: "button",
    buttonImage: "../../static/images/iconos/calendar.gif",
    buttonImageOnly: true,
    closeText: 'Cerrar',
    prevText: '&#x3c;Ant',
    nextText: 'Sig&#x3e;',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
        'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi&eacute;rcoles', 'Jueves', 'Viernes', 'S&aacute;bado'],
    dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Juv', 'Vie', 'S&aacute;b'],
    dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S&aacute;'],
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    //    defaultDate: "+1w",
    //    weekHeader: 'Sm',
    //    showWeek: true,
    changeMonth: true,
    changeYear: true,
    yearRange: "-99:+0", //rango de años
    //    minDate: '0',
    numberOfMonths: 1,
    onSelect: function() {
        $("#txtFechaNacimiento").removeClass("warning");
        var birthDate = new Date($("#txtFechaNacimiento").datepicker("getDate"));
        calcular_edad(birthDate);
    }
});

(function($) {
    $.fn.rotateTableCellContent = function(options) {
        /*
         Version 1.0
         7/2011
         Written by David Votrubec (davidjs.com) and
         Michal Tehnik (@Mictech) for ST-Software.com
         */

        var cssClass = ((options) ? options.className : false) || "vertical";

        var cellsToRotate = $('.' + cssClass, this);

        var betterCells = [];
        cellsToRotate.each(function() {
            var cell = $(this)
                    , newText = cell.text()
                    , height = cell.height()
                    , width = cell.width()
                    , newDiv = $('<div>', {height: width, width: height})
                    , newInnerDiv = $('<div>', {text: newText, 'class': 'rotated'});

            newInnerDiv.css('-webkit-transform-origin', (width / 2) + 'px ' + (width / 2) + 'px');
            newInnerDiv.css('-moz-transform-origin', (width / 2) + 'px ' + (width / 2) + 'px');
            newDiv.append(newInnerDiv);

            betterCells.push(newDiv);
        });

        cellsToRotate.each(function(i) {
            $(this).html(betterCells[i]);
        });
    };
})(jQuery);

function Restringido(e) {
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8 || (tecla == 0))
    {
        return true;
    }
    var patron;
    patron = /[1-10]/
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);

}

function Alfanumerico(e) {
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8 || (tecla == 0))
    {
        return true;
    }
    var patron;
    patron = /[a-zA-Z Ññüáéíó]/
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}



function campoFechaValido(elemento) {
    if (validarFecha(elemento.val())) {
        $("#sp" + elemento.attr('id').substring(3, 30)).removeClass("ui-icon ui-icon-close ui-state-error ui-corner-all").addClass("ui-icon ui-icon-check");
    } else {
        $("#sp" + elemento.attr('id').substring(3, 30)).removeClass("ui-icon ui-icon-check").addClass("ui-icon ui-icon-close ui-state-error ui-corner-all");
    }
}

function validarFecha(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtDay = dtArray[1];
    dtMonth = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2)
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}