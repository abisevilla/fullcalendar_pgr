<%-- 
    Document   : Testing
    Created on : 17-oct-2016, 20:45:01
    Author     : WNRO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--<link  type="image/x-icon" href="../../static/images/iconos/escudo.ico" rel="shortcut icon"/>-->
        <!--        <link type="text/css"  href="../../static/css/webstyle.css">
                <link  type="text/css" href="../../static/css/jquery-ui-1.8.23.custom.css">
                <link  type="text/css" href="../../static/css/ui.jqgrid.css">
                <link  type="text/css" href="../../static/css/menu_style.css">-->

        <link rel="stylesheet" type="text/css" href="../../static/css/webstyle.css">
        <link rel="stylesheet" type="text/css" href="../../static/css/jquery-ui-1.8.23.custom.css">
        <link rel="stylesheet" type="text/css" href="../../static/css/ui.jqgrid.css">
        <link rel="stylesheet" type="text/css" href="../../static/css/menu_style.css">



        <script type="text/javascript" src="../../static/js/config/jquery.js"></script>
        <script  type="text/javascript" src="../../static/js/config/jqueryui.js"></script>
        <script  type="text/javascript" src="../../static/js/config/jquery.maskedinput-1.3.js"></script>
        <script type="text/javascript"   src="../../static/js/config/jstepper.js"></script>
        <script  type="text/javascript"   src="../../static/js/config/jquery.jqGrid.min.js"></script>
        <script  type="text/javascript" src="../../static/js/config/grid.locale-es.js"></script>
        <script>
            $(document).ready(function () {
                $.getScript("../../static/js/general/Testing1.js");
            });
        </script>
        <style>
            .ui-jqgrid tr.jqgrow td {
                white-space: normal !important;
            }
        </style>
    </head>
    <body>

        <div class="wrapper">
            <div class="shadowcontainer">
                <div>
                    <div class="maincontainer" >
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Top.jspf"/>--%>
                        </div>
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Menu.jspf"/>--%>
                        </div>
                        <div>
                            <div class="workareacontainer ui-widget-content">
                                <div class="container">
                                    <!-- Opcion de informacion -->
                                    <div id="divMensajeSistema" style="display:none" class="ui-widget">
                                        <div id="divNoticiaSistema" class="">
                                            <table>
                                                <tr>
                                                    <td> <span id="iconoNoticiaSistema" style=" margin-right: .3em;" class=""></span></td>
                                                    <td> <span id="textoNoticiaSistema" ></span></td>
                                                    <td style="text-align:center; margin-left:55%; border-left-style:solid">Cerrar Mensaje</td>
                                                    <td style="text-align:center"> <span id="cerrarNoticiaSistema" ></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- fin de opcion de información.-->
                                </div>
                                <div class="container" >
                                    <div style="text-align: center" ><h1>EVALUACION DE COMPETENCIA TECNICA Y COMPORTAMIENTO LABORAL</h1></div>
                                </div>
                                <div class="container">
                                    <div id="tabsBusqueda">
                                        <ul>
                                            <li><a href="#busqueda1">Empleado</a></li>
                                            <!--
                                            <li><a href="#busqueda2">Procuradurias</a></li>
                                            <li><a href="#busqueda3">Unidad</a></li>-->
                                        </ul>
                                        <div id="busqueda1">
                                            <p>
                                                Para realizar la busqueda de un empleado escriba el nombre del empleado(nombre o apellido) y se realizara una busqueda automatica
                                            </p>
                                            <p>
                                                Nombre Empleado(a): <input type="text" id="txtBusquedaEmpleado" placeholder="Ej: CARLOS PEREZ" size="55" maxlength="40" onKeyUp="this.value = this.value.toUpperCase();" onKeyPress="return Alfanumerico(event)"/>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="container" id="divEncabezadoFormulario">
                                    <table id="tbDatosRegistros"  border="1" cellpadding="0" cellspacing="1" bordercolor="#000000" style="border-collapse:collapse; border-color: #aaaaaa;">
                                        <tr>
                                            <td>
                                                Formulario:
                                            </td>
                                            <td colspan="5" style="text-align: left">
                                                <select id="slRecursosHumanos" name="slRecursosHumanos">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nombre Empleado:</td>
                                            <td><input type="text" id="txtNombreEmpleado" name="txtNombreEmpleado" size="40" readonly="true" /></td>
                                            <td colspan="2">Apellido Empleado:</td>
                                            <td><input type="text" id="txtApellidoEmpleado" name="txtApellidoEmpleado" size="40" readonly="true"/></td>
                                            <td style="text-align:  center">
                                                <input type="text" style="text-align: center" id="txtCodigoEmp" name="txtCodigoEmp" size="7" readonly="true" />
                                                <button id="cmdEditarEmpleado">Editar</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cargo</td>
                                            <td colspan="2"><input type="text" id="txtCargoEmpleado" name="txtCargoEmpleado" size="45" readonly="true"/></td>
                                            <td>Unidad:</td>
                                            <td colspan="2"><input type="text" id="txtUnidadEmpleado" name="txtUnidadEmpleado" size="45" readonly="true"/></td>
                                        </tr>
                                        <tr>
                                            <td>Procuraduria Auxiliar:</td>
                                            <td colspan="3"><input type="text" id="txtProcuraduriaEmpleado" name="txtProcuraduriaEmpleado" size="50" readonly="true"/></td>
                                            <td>Tipo Acta</td>
                                            <td>
                                                <select id="slTipoEvaluacion" name="slTipoEvaluacion">
                                                    <option value="ANUAL">EVALUACION ANUAL</option>
                                                    <option value="MIGRACION">MIGRACION</option>
                                                    <option value="NUEVO">NUEVO INGRESO</option>
                                                    <option value="REVISION">REVISION</option>
                                                    <option value="SEGUIMIENTO">SEGUIMIENTO</option>
                                                    <option value="TRASLADO">TRASLADO</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: right">
                                                <button id="cmdLimpiarTodo" class="">Limpiar Todo</button>
                                            </td>
                                            <td colspan="3" style="text-align: left">
                                                <button id="cmdCargarFormulario" title="">Cargar Formulario</button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="container" id="divEdicionDatos" style="text-align: center">
                                    <table border="1" cellpadding="5" cellspacing="1" bordercolor="#000000" style="border-collapse:collapse; border-color: #aaaaaa;">
                                        <tr>
                                            <td colspan="4">
                                                <b>EDICION DE DATOS DE EMPLEADOS DE LA PGR</b>
                                                <input type="text" style="text-align: center" id="txtEditarCodigoEmp" name="txtEditarCodigoEmp" size="7" readonly="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nombre:</td>
                                            <td style="text-align: left">
                                                <input type="text" id="txtnombreEditar" name="txtnombreEditar" size="45" onKeyUp="this.value = this.value.toUpperCase();" />
                                            </td>

                                            <td>
                                                <button id="cmdGuardarNombre">Guardar</button>
                                            </td>
                                            <td rowspan="5">
                                                <button id="cmdCancelarEdicion">Regresar</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Apellidos</td>
                                            <td style="text-align: left">
                                                <input type="text" id="txtApellidosEditar" name="txtApellidosEditar"  size="55" onKeyUp="this.value = this.value.toUpperCase();"/>
                                            </td>

                                            <td>
                                                <button id="cmdGuardarApellido">Guardar</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cargo:</td>
                                            <td style="text-align: left">
                                                <input type="hidden" id="txtCodigoCargo" name="txtCodigoCargo" readonly="true" />
                                                <input  type="text" id="txtNombreCargo" name="txtNombreCargo" size="55" onKeyUp="this.value = this.value.toUpperCase();"/>


                                            </td>

                                            <td>
                                                <button id="cmdGuardarCargo">Guardar</button>
                                            </td>
                                        </tr>
                                        <!--<tr>
                                            <td>Procuraduria:</td>
                                            <td style="text-align: left">
                                                <select id="slNombreProcuraduria" name="slNombreProcuraduria"></select>
                                                
                                            </td>
                                            
                                            <td>
                                                <button id="cmdGuardarProcuraduria">Guardar</button>
                                            </td>
                                        </tr>-->
                                        <tr>
                                            <td>Unidad:</td>
                                            <td style="text-align: left">
                                                <input type="hidden" id="txtCodigoUnidad" name="txtCodigoCargo" readonly="true" />
                                                <input size="55" type="text" id="txtNombreUnidad" name="txtNombreUnidad" onKeyUp="this.value = this.value.toUpperCase();" />
                                            </td>

                                            <td>
                                                <button id="cmdGuardarUnidad">Guardar</button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="container">
                                    <div id="divComportamiento"></div>

                                </div>
                                <div class="container">
                                    <div id="divHabilidades"></div>
                                </div>
                                <div class="container">
                                    <div id="divMatriz">
                                    </div>
                                </div>
                                <div class="container">
                                    <div id="divObservaciones" hidden="false">
                                        <table style="">
                                            <tr>
                                                <td><b>Observaciones de la persona:</b></td>
                                                <td colspan="3"><input id="txtObservaciones" name="txtObservaciones" type="text" size="100" maxlength="500" placeholder="Observaciones" onKeyUp="this.value = this.value.toUpperCase();" /></td>

                                            </tr>
                                            <tr>
                                                <td><b>Areas por mejorar</b> </td>
                                                <td colspan="3"><input id="txtAreasMejorar" name="txtAreasMejorar" type="text" size="100" maxlength="500" placeholder="Area(s) por mejorar" onKeyUp="this.value = this.value.toUpperCase();" /></td>

                                            </tr>
                                            <tr>
                                                <td><b>Nombre del Evaluador:</b> </td>
                                                <td><input id="txtNombreEvaluador" name="txtNombreEvaluador" type="text" size="100" maxlength="500" placeholder="Ej. Carlos Perez" onKeyUp="this.value = this.value.toUpperCase();" /></td>
                                                <td><input id="txtCodigoEvaluador" name="txtCodigoEvaluador" type="text" size="6" readonly="true" style="text-align: center"/></td>
                                                <td><button id="cmdCancelarEvaluador"></button></td>
                                            </tr>
                                            <tr>
                                                <td><b>Fecha de Evaluacion:</b> </td>
                                                <td colspan="3"><input id="txtFechaEvaluado" name="txtFechaEvaluado" type="text"  style="text-align: center"/></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>    
                                <div class="container">
                                    <div id="divOpcionesFormulario" hidden="true" style="text-align: center">
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <button title="Enviar Formulario" id="cmdGuardarFormulario">Guardar</button>
                                                </td>
                                                <td>
                                                    <button title="Vuelve a cero las casillas de evaluación" id="cmdLimpiarFormulario">Colocar Ceros</button>
                                                </td>
                                                <td>
                                                    <button  class="" title="Cancelar el registro. El formulario no aparecerá en pantalla" id="cmdCancelarFormulario">Cancelar</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divCargando" class="ui-state-highlight ui-corner-all">

                                    </div>
                                </div>
                                <div class="container">
                                    <!-- Opcion de informacion -->
                                    <div id="divInformativo" style="display:none" class="ui-widget">
                                        <div id="divNoticia" class="">
                                            <table>
                                                <tr>
                                                    <td> <span id="iconoNoticia" style=" margin-right: .3em;" class=""></span></td>
                                                    <td> <span id="textoNoticia" ></span></td>
                                                    <td style="text-align:center; margin-left:55%">. Cerrar Mensaje</td>
                                                    <td style="text-align:center"> <span id="cerrarNoticia" ></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- fin de opcion de información.-->
                                </div>
                            </div>
                        </div>
                        <div>
                            <%--<jsp:directive.include file="../../plantilla/Footer.jspf"/>--%>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </body>
</html>
